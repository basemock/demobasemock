using KS.DomainServices.Services;
using KS.Infrastructure.Context;
using KS.Infrastructure.Interfaces;
using KS.Infrastructure.Implementation;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container
builder.Services.AddTransient<IUserService, UserService>();
builder.Services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
builder.Services.AddTransient<IUserRepository, UserRepository>();
builder.Services.AddTransient<IUnitOfWork, UnitOfWork>();

var connection = builder.Configuration.GetConnectionString("WalletConnection");

builder.Services.AddDbContext<WalletContext>(x => x.UseMySql(connection, ServerVersion.Parse("8.4.0-mysql")));



// Add AutoMapper
builder.Services.AddAutoMapper(typeof(KS.WebApi.Mappings.AutoMapperProfile));

// Add services to the container.   
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
