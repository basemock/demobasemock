﻿using AutoMapper;
using KS.DomainServices.Dtos;
using KS.DomainServices.Services;
using KS.Infrastructure.Entities;
using KS.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KS.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UsersController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers() {
            var user = await _userService.GetUserAsync();
            return Ok(user);
        }
        [HttpGet("{Id}")]
        public async Task<IActionResult> GetUserById(Guid Id)
        {
            var user = await _userService.GetUserByIdAsync(Id);
            return Ok(user);
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser(UserDto userDto)
        {
            var user = _mapper.Map<User>(userDto);
            await _userService.CreateUserAsync(user);
            return Ok();
        }
        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateUser(Guid Id, UserDto userDto)
        {
            try
            {
                if (userDto == null)
                    return BadRequest("User is null");
                if (!ModelState.IsValid)
                    return BadRequest("Invalid");
                var userEntity = await _userService.GetUserByIdAsync(Id);
                if (userEntity == null)
                    return NotFound();
                _mapper.Map(userDto, userEntity);
                await _userService.UpdateUserAsync(userEntity);
                return Ok();
            }
            catch (Exception) {
                throw new Exception();
            }
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> RemoveUser(Guid Id)
        {
            var user = await _userService.GetUserByIdAsync(Id);
            await _userService.RemoveUserAsync(user);
            return Ok();
        }
    }
}