﻿using KS.Infrastructure.Context;
using KS.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.Infrastructure.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        private WalletContext _context;
        private IUserRepository _user;
        public UnitOfWork(WalletContext context)
        {
            _context = context;
        }
        public IUserRepository UserRepository {
            get
            {
                if(_user != null)
                {
                    return _user;
                }
                return new UserRepository(_context);
            }
        }

        public void save()
        {
            _context.SaveChanges();
        }
    }
}