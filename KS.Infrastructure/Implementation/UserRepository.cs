﻿using KS.Infrastructure.Context;
using KS.Infrastructure.Entities;
using KS.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.Infrastructure.Implementation
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(WalletContext context) : base(context)
        {
        }
    }
}
