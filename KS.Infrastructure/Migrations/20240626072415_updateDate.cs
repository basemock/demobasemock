﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace KS.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class updateDate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2024, 6, 26, 7, 24, 13, 460, DateTimeKind.Utc).AddTicks(4389),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2024, 6, 14, 20, 40, 4, 122, DateTimeKind.Local).AddTicks(9563));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2024, 6, 26, 7, 24, 13, 460, DateTimeKind.Utc).AddTicks(2795),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2024, 6, 14, 20, 40, 4, 122, DateTimeKind.Local).AddTicks(8871));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2024, 6, 14, 20, 40, 4, 122, DateTimeKind.Local).AddTicks(9563),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2024, 6, 26, 7, 24, 13, 460, DateTimeKind.Utc).AddTicks(4389));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2024, 6, 14, 20, 40, 4, 122, DateTimeKind.Local).AddTicks(8871),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2024, 6, 26, 7, 24, 13, 460, DateTimeKind.Utc).AddTicks(2795));
        }
    }
}
