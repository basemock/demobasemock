﻿
using AutoMapper;
using KS.DomainServices.Dtos;
using KS.Infrastructure.Entities;

namespace KS.WebApi.Mappings
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
           // CreateMap<UserDto, UserForUpdate>();
        }
    }
}
