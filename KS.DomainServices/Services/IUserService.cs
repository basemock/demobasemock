﻿using KS.DomainServices.Dtos;
using KS.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.DomainServices.Services
{
    public interface IUserService
    {
        Task CreateUserAsync(User user);
        Task<List<User>> GetUserAsync();
        Task<User> GetUserByIdAsync(Guid Id);
        Task UpdateUserAsync(User user);
        Task RemoveUserAsync(User user);
    }
}
