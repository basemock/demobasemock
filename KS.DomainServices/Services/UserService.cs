﻿using KS.DomainServices.Dtos;
using KS.Infrastructure.Entities;
using KS.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.DomainServices.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task CreateUserAsync(User user)
        {

            await _unitOfWork.UserRepository.AddAsync(user);
        }
        public async Task<List<User>> GetUserAsync()
        {
            var UserList = await _unitOfWork.UserRepository.GetAllAsync();
            return UserList.ToList();
        }

        public async Task<User> GetUserByIdAsync(Guid Id)
        {
            var user = await _unitOfWork.UserRepository.GetByIdAsync(Id);
            return user;
        }
        public async Task UpdateUserAsync(User user)
        {
            await _unitOfWork.UserRepository.Update(user);
            _unitOfWork.save();
        }
        public async Task RemoveUserAsync(User user)
        {

            await _unitOfWork.UserRepository.Remove(user);
            _unitOfWork.save();
        }
    }
}
