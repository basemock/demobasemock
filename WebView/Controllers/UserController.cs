﻿using KS.DomainServices.Dtos;
using KS.Infrastructure.Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Http.Json;
using System.Text;
using WebView.EndPoint;


namespace WebView.Controllers
{
    public class UserController : Controller
    {
        Uri baseUrl = new Uri("https://localhost:44317/api");
        string url = "https://localhost:44317/api";

        private readonly HttpClient _httpClient;
        public UserController()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = baseUrl;
        }
        //[Route("UserList")]
        public IActionResult Index()
        {
            List<UserDto> userList = new List<UserDto>();
            HttpResponseMessage res = _httpClient.GetAsync(_httpClient.BaseAddress + ApiPath.GETUSER).Result;
            if (res.IsSuccessStatusCode) {

                var user = res.Content.ReadAsStringAsync().Result;
                userList = JsonConvert.DeserializeObject<List<UserDto>>(user);
            }
            return View(userList.OrderByDescending(i => i.CreatedDate));
        }
        public IActionResult Create()
        {
            
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create([Bind ("UserName, FullName")] UserDto user)
        {
            string url = "https://localhost:44317/api/Users/CreateUser";
            try
            {
                string data = JsonConvert.SerializeObject(user);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage res = await _httpClient.PostAsync(url, content);
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
            return View();
        }
 
        public async Task<IActionResult> Edit(Guid Id)
        {
            var userUpdate = new UserDto();
            HttpResponseMessage res = await _httpClient.GetAsync($"https://localhost:44317/api/Users/GetUserById/{Id}");

            if (res.IsSuccessStatusCode) {
                var userRes = await res.Content.ReadAsStringAsync();
                userUpdate = JsonConvert.DeserializeObject<UserDto>(userRes);
                //Console.WriteLine($"user res {userRes} user update {userUpdate}");
            }
            return View(userUpdate);

        }
        [HttpPost]
        public async Task<IActionResult> Edit([Bind("Id, UserName, FullName")] UserDto user)
        {
            try
            {
                string data = JsonConvert.SerializeObject(user);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage res = await _httpClient.PutAsync($"https://localhost:44317/api/Users/UpdateUser/{user.Id}", content);
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            catch(Exception)
            {
                throw new Exception();
            }
            return View(user);
        }
        public async Task<IActionResult> Delete(Guid Id)
        {
            var userUpdate = new UserDto();
            HttpResponseMessage res = await _httpClient.GetAsync($"https://localhost:44317/api/Users/GetUserById/{Id}");

            if (res.IsSuccessStatusCode)
            {
                var userRes = await res.Content.ReadAsStringAsync();
                userUpdate = JsonConvert.DeserializeObject<UserDto>(userRes);
                //Console.WriteLine($"user res {userRes} user update {userUpdate}");
            }
            return View(userUpdate);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(UserDto user)
        {
            try
            {
                string data = JsonConvert.SerializeObject(user);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage res = await _httpClient.DeleteAsync($"https://localhost:44317/api/Users/RemoveUser/{user.Id}");
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
            return View(user);
        }

    }
}
